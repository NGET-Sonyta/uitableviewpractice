//
//  ViewController.swift
//  UITableViewControllerPractice
//
//  Created by Nyta on 11/17/20.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    var flower: [[String]] = [["Animal", "Kitten", "Puppy"],["Flower","Jasmine ", "Rose"], ["Fruit","Apple","Banana"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        table.delegate = self
        table.dataSource = self
    }


}

extension ViewController: UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return flower.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return flower[section][0]
    }
}

extension ViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flower[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        var data = flower[indexPath.section][indexPath.row]
        


        if indexPath.row != 0 {
            cell.textLabel?.text = data
        }
          return cell

    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {
                self.flower[indexPath.section].remove(at: indexPath.row)
                tableView.reloadData()
            }
    }
}
